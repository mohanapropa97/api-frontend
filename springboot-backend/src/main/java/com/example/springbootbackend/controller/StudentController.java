package com.example.springbootbackend.controller;
import com.example.springbootbackend.dto.StudentDto;
import com.example.springbootbackend.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/students")
@AllArgsConstructor
public class StudentController {

     private final StudentService studentService;
    @PostMapping("/createStudent")
    public ResponseEntity<?> createStudent(@RequestBody StudentDto studentDto ) {
       String message = studentService.createStudent(studentDto);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }


    @GetMapping("/getAllStudent")
    public ResponseEntity<List<StudentDto>> getAllStudents() {
        List<StudentDto> students = studentService.getAllStudents();
        return ResponseEntity.ok(students);
    }

    @PutMapping("/updateStudent/{id}")
    public ResponseEntity<?> updateStudent(@RequestBody StudentDto newStudentDto, @PathVariable Long id)
    {
        String message=studentService.updateStudent(newStudentDto,id);
        return new ResponseEntity<>(message,HttpStatus.OK);
    }
    @DeleteMapping("/deleteStudent/{id}")
    public ResponseEntity<?> deleteStudentById(@PathVariable Long id)
    {
        String message = studentService.deleteStudentById(id);
        return new ResponseEntity<>(message,HttpStatus.OK);
    }

}
