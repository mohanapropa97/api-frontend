package com.example.springbootbackend.service;

import com.example.springbootbackend.dto.StudentDto;
import com.example.springbootbackend.entity.Student;
import com.example.springbootbackend.repository.StudentRepository;
import lombok.AllArgsConstructor;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.*;

@Service
@AllArgsConstructor
public class StudentService {

   public final StudentRepository studentRepository;

   public String createStudent(StudentDto studentDto)
    {
        if(studentRepository.existsByEmail(studentDto.getEmail()))
        {

            return "Parent is already Exist";

        }
        else{
            Student student = new Student();

           BeanUtils.copyProperties(studentDto,student);
           studentRepository.save(student);
           return "Student is Created";

        }
    }

    public List<StudentDto> getAllStudents() {
        List<Student> studentList = studentRepository.findAll();
        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student: studentList) {
            StudentDto studentDto = entityToDto(student);
            studentDtoList.add(studentDto);
        }
        return studentDtoList;
    }

  public String updateStudent(StudentDto newStudentDto,Long id)
  {
      Optional<Student> optionalStudent=studentRepository.findById(id);
      System.out.println(optionalStudent);
      if(optionalStudent.isPresent())
      {
          Student student =optionalStudent.get();
          student.setFirstName(newStudentDto.getFirstName());
          student.setLastName(newStudentDto.getLastName());
          student.setEmail(newStudentDto.getEmail());
          studentRepository.save(student);
          entityToDto(student);
          return "Student with ID " + id + " is updated.";
      }
      else
      {
          return "Student with ID " + id + " not found";
      }
  }

  public String deleteStudentById(Long id)
  {
      Optional<Student> optionalStudent = studentRepository.findById(id);
      if(optionalStudent.isPresent())
      {

          Student student = optionalStudent.get();
          studentRepository.delete(student);
          return "Student with ID " + id + " is Deleted.";
      }
      else
      {
          return "Student with ID " + id + " not found";
      }
  }



    private StudentDto entityToDto(Student student){
        StudentDto studentDto = new StudentDto();
        BeanUtils.copyProperties(student, studentDto);
        return studentDto;
    }



}
