package com.example.springbootbackend.dto;

import lombok.Data;

@Data
public class StudentDto {
    private long id;
    private String firstName;
    private String lastName;
    private String email;

}
