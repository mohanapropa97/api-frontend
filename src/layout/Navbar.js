import React from 'react'
import {Link} from "react-router-dom";

export default function Navbar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
     <div className="container-fluid">
    <a className="navbar-brand" href="#">Full Stack Student API</a>
    <Link className="btn btn-outline-dark" to="/">Get Student</Link>
    <Link className="btn btn-outline-light t" to="/adduser">Add Student</Link>
  </div>
</nav>
  )
}
