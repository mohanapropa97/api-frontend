import React, { useEffect, useState } from 'react'
import axios from 'axios';
import {Link, useParams, } from 'react-router-dom'

export default function Home() {

    const [users, setUsers]=useState({});
    const { id } = useParams;

    useEffect(()=>{
        loadUser();
    },[]);

    const loadUser = async () => {
        
            const result = await axios.get("http://localhost:8080/students/getAllStudent");
            setUsers(result.data);
       };

       const deleteUser = async(id)=>{
        loadUser();
        await axios.delete(`http://localhost:8080/students/deleteStudent/${id}`)
       }

  return (
    <div className="container">
        <div className="py-4">
        <table className="table border shadownp">
  <thead>
    <tr>
      <th scope="col">List</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Email</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
 
  <tbody>
    {
        Object.values(users).map((user,index) => (
            <tr>
                <th scope="row" key={index}>{index+1}</th>
                <td>{user.firstName}</td>
                <td>{user.lastName}</td>
                <td>{user.email}</td>
                <td>
                  <button className="btn btn-primary mx-2">View</button>
                  <Link className="btn btn-outline-primary mx-2"
                  to={`/edituser/${user.id}`}>Edit</Link>
                  <button className="btn btn-danger mx-1" 
                  onClick={()=>deleteUser(user.id)}
                  >Delete</button>
                </td>
            </tr>

        ))
    }
   
  </tbody>
</table>
        </div>
    </div>
  )
}
