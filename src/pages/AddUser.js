import axios from 'axios';
import React from 'react'
import { useState } from 'react';
import {useNavigate} from "react-router-dom";


export default function AddUser() {

    let navigate = useNavigate();
const [user, setUser] =useState({
    firstName:"",
    lastName:"",
    email:""
});
const {firstName,lastName,email}=user;

const onInputChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  const onSubmit =async(e)=>{
    e.preventDefault();
    console.log(user);
    await axios.post("http://localhost:8080/students/createStudent",user);
    navigate("/");
  }


  return (
    <div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center">Register Student</h2>
                <form onSubmit={(e)=>onSubmit(e)}>
                
                <div className="mb-3">
                    <lebel htmlFor="firstname" className="form-lebel">
                    First Name
                    </lebel>
                    <input
                    type={"text"}
                    className="form-control"
                    placeholder="Enter First Name"
                    name="firstName"
                    value={firstName}
                    onChange={(e)=>onInputChange(e)}/>
                    
                </div>

                <div className="mb-3">
                    <lebel htmlFor="lastName" className="form-lebel">
                       Last Name
                    </lebel>
                    <input
                    type={"text"}
                    className="form-control"
                    placeholder="Enter Last Name"
                    name="lastName"
                    value={lastName}
                    onChange={(e)=>onInputChange(e)}/>
                    
                </div>
                <div className="mb-3">
                    <lebel htmlFor="Email" className="form-lebel">
                       E-mail
                    </lebel>
                    <input
                    type={"text"}
                    className="form-control"
                    placeholder="Enter Student Email"
                    name="email"
                    value={email}
                    onChange={(e)=>onInputChange(e)}/>
                    
                </div>
                <button type="submit" className="btn btn-outline-primary">Submit</button>
                <button type="submit" className="btn btn-outline-danger mx-2">Cancel</button>
                </form>

            </div>
        </div>
       
    </div>
  )
}
